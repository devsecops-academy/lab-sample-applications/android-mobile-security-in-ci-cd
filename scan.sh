#!/bin/sh

#Define the API key to use for API interaction
MOBSF_API_KEY=df294c26-d37d-425c-90e6-d0f61e3c0736
FILE_NAME="file=@$1"

#Upload the file by using the variable $FILE_NAME defined above
response=`...`

#Define variables to retain data
#The first variable is already configured. Use it as an example
#Use jq to extract only the necessary information from the response of the upload request
SCAN_TYPE=`echo $response | jq '.scan_type' | sed 's/"//g'`
FILE_NAME=`... | sed 's/"//g'`
HASH=`... | sed 's/"//g'`

#Perform the scan using the defined variables and save the output to a JSON file
scan_response=`...`
echo $scan_response | grep -Po '"security_score":[^"]+'

#Save the output as a PDF file too for improved readability
curl -X POST --url http://localhost:8000/api/v1/download_pdf --data "hash=$HASH" -H "Authorization:$MOBSF_API_KEY" --output ./reports/report.pdf
